<?php

define('BASE_APP_DIR', 'src/');
define('APP_CLASS_EXTENSION', '.php');

spl_autoload_register(function($name){
    require_once BASE_APP_DIR . str_replace('\\', '/', $name) . APP_CLASS_EXTENSION;
});