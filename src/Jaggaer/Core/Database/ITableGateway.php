<?php

namespace Jaggaer\Core\Database;

interface ITableGateway
{
    function setConnection(\PDO $conn);
    function getConnection();

    function setTableName($name);
    function getTableName();

    function readAll($delegate = false);
    
    function iterate($callback);

    function getById($id);
    function getOneBy($columnName, $value);

    function search(array $params, $delegate = false);

    function create(array $data);
    function update($id, array $data);
    function delete($id);

    function deleteAll();
}
