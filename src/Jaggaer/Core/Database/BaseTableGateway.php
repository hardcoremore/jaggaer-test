<?php

namespace Jaggaer\Core\Database;

use Jaggaer\Core\Database\ITableGateway;
use Jaggaer\Core\Database\DatabaseErrorException;

class BaseTableGateway implements ITableGateway
{
    protected $databaseConnection;
    protected $tableName;
    protected $idColumnName;
    protected $lastResult;

    public function setConnection(\PDO $connection)
    {
        $this->databaseConnection = $connection;
    }

    public function getConnection()
    {
        return $this->databaseConnection;
    }

    public function setTableName($name)
    {
        $this->tableName = $name;
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function setIdColumnName($name)
    {
        $this->idColumnName = $name;
    }

    public function getIdColumnName()
    {
        return $this->idColumnName;
    }

    public function iterate($callback)
    {
        $resultArray = [];

        // PDO statement implements traversable so no need to use yield
        foreach($this->lastResult as $row)
        {
            $callback($row);
        }
    }

    public function readAll($delegate = false)
    {
        $this->lastResult = $this->databaseConnection->query('SELECT * FROM ' . $this->tableName);

        if($this->lastResult)
        {
            if($delegate)
            {
                return $this;
            }
            else
            {
                return $this->lastResult->fetchAll(\PDO::FETCH_ASSOC);
            }
        }
        else
        {
            $this->throwDatabaseErrorException();
        }
    }
    
    public function getById($id)
    {
        $this->lastResult = $this->databaseConnection->prepare('SELECT * FROM ' . $this->tableName . ' WHERE ' . $this->idColumnName .' = :id');

        $isComplete = $this->lastResult->execute(['id' => $id]);

        if($isComplete)
        {
            return $this->lastResult->fetch(\PDO::FETCH_ASSOC);
        }
        else
        {
            $this->throwDatabaseErrorException();
        }
    }

    public function getOneBy($columnName, $value)
    {
        $this->lastResult = $this->databaseConnection->prepare('SELECT * FROM ' . $this->tableName . ' WHERE ' . $columnName .' = :value');

        $isComplete = $this->lastResult->execute(['value' => $value]);

        if($isComplete)
        {
            return $this->lastResult->fetch(\PDO::FETCH_ASSOC);
        }
        else
        {
            $this->throwDatabaseErrorException();
        }
    }

    public function search(array $searchParams, $delegate = false)
    {
        $sqlString = 'SELECT * FROM ' . $this->tableName . ' WHERE ';

        if(count($searchParams) > 0)
        {
            $whereString = '';
            $params = [];

            foreach($searchParams as $key => $val)
            {
                $whereString .= $key;

                if(is_array($val))
                {
                    $whereString .= ' '. $val[0] . ' :' . $key;
                    $params[$key] = $val[1];
                }
                else
                {
                    $whereString .= ' = :' . $key;   
                    $params[$key] = $val;
                }

                $whereString .= ' AND ';
            }

            $sqlString .= rtrim($whereString, ' AND ');

            $this->lastResult = $this->databaseConnection->prepare($sqlString);
            $isComplete = $this->lastResult->execute($params);

            if($this->lastResult)
            {
                if($delegate)
                {
                    return $this;
                }
                else
                {
                    return $this->lastResult->fetchAll(\PDO::FETCH_ASSOC);
                }
            }
            else
            {
                $this->throwDatabaseErrorException();
            }
        }
        else
        {
            return [];
        }
    }

    public function create(array $data)
    {
    }

    public function update($id, array $data)
    {
    }

    public function delete($id)
    {
    }

    public function deleteAll()
    {
    }
    
    protected function throwDatabaseErrorException()
    {
        throw new DatabaseErrorException(implode(' - ', $this->databaseConnection->errorInfo()));
    }
}