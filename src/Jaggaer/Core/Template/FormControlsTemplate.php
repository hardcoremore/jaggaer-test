<?php

namespace Jaggaer\Core\Template;

use Jaggaer\Core\Template;

class FormControlsTemplate extends BaseHtmlTemplate
{

    private $template = <<<EOF

<div class="form-field-set form-controls-set {{formControlsClass}}">

    <div class="form-controls">

        <span class="button save-form-button">
            <i class="glyphicon save-button-icon"></i>
            <span class="button-label">{{saveFormButtonLabel}}</span>
        </span>

        <span class="button cancel-form-button">
            <i class="glyphicon cancel-button-icon"></i>
            <span class="button-label">{{cancelFormButtonLabel}}</span>
        </span>

    </div>

</div>

EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}