<?php

namespace Jaggaer\Core\Template;

use Jaggaer\Core\Template;

class FormTemplate extends BaseHtmlTemplate
{
    public function __construct()
    {
        parent::__construct();

        $this->setProperty('saveFormButtonLabel', 'Save');
        $this->setProperty('cancelFormButtonLabel', 'Cancel');
    }

    private $template = <<<EOF
<form method="post" id="{{id}}" class="crud-module-form {{formClass}}">

    {%formInputs%}

    {%formControls%}

</form>
EOF;    

    public function getTemplate()
    {
        return $this->template;
    }
}