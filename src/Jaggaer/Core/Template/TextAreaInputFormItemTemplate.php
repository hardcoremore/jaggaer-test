<?php

namespace Jaggaer\Core\Template;

use Jaggaer\Core\Template;

class TextAreaInputFormItemTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
<div class="form-field-set {{class}}">
    <span class="form-field-error form-field-error-{{name}}" data-field-name="{{name}}"></span>
    <label class="form-field-label" for="{{name}}-{{nameSpace}}-input">{{label}}</label>
    <input type="password" class="form-field-input" name="{{name}}" id="{{name}}-{{nameSpace}}-input">
</div>

<textarea id="{{name}}-{{nameSpace}}-input" name="{{ name }}" class="form-field-input"></textarea>
EOF;

    public function getTemplateTemplate()
    {
       return $this->template;
    }
}
