<?php

namespace Jaggaer\Core\DependencyInjection;

use Jaggaer\Core\DependencyInjection\ServiceDefinition;

interface IDependencyContainer
{
    function getServiceDefinition($serviceName);
    function registerService($serviceName, ServiceDefinition $service);
    function getService($serviceName, $newInstance = false);
}