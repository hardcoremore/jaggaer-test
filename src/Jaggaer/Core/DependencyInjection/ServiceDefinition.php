<?php

namespace Jaggaer\Core\DependencyInjection;

class ServiceDefinition
{
    public $className;
    public $constructArguments;
    public $methodArguments;
    public $isStatic;

    public function __construct($className, $constructArguments = null, $methodArguments = null, $isStatic = true)
    {
        $this->className = $className;
        $this->constructArguments = $constructArguments;
        $this->methodArguments = $methodArguments;
        $this->isStatic = $isStatic;
    }
}