<?php

namespace Jaggaer\Core\DependencyInjection;

use Jaggaer\Core\DependencyInjection\IDependencyContainer;
use Jaggaer\Core\DependencyInjection\ServiceDefinition;

use Jaggaer\Core\DependencyInjection\ServiceNotFoundException;

class DependencyContainer implements IDependencyContainer
{
    private $serviceDataList;
    private $serviceInstances;

    private $propertyList;

    public function __construct()
    {
        $this->propertyList = [];

        $this->serviceDataList = [];
        $this->staticServiceInstances = [];
    }

    public function setProperty($propertyName, $value)
    {
        if(strlen($propertyName) > 0 && $value)
        {
            $this->propertyList[$propertyName] = $value;
        }
    }

    public function getProperty($propertyName, $defaultValue = null)
    {
        if(array_key_exists($propertyName, $this->propertyList))
        {
            return $this->propertyList[$propertyName];
        }
        else
        {
            return $defaultValue;
        }
    }

    public function getServiceDefinition($serviceName)
    {
        if(array_key_exists($serviceName, $this->serviceDataList))
        {
            return $this->serviceDataList[$serviceName];
        }
        else
        {
            return null;
        }
    }

    public function registerService($serviceName, ServiceDefinition $service)
    {
        if(strlen($serviceName) > 0 && $service)
        {
            $this->serviceDataList[$serviceName] = $service;
        }
    }

    public function setService($serviceName, $service)
    {
        $this->staticServiceInstances[$serviceName] = $service;
    }

    public function getService($serviceName, $newInstance = false)
    {
        $serviceDefinition = $this->getServiceDefinition($serviceName);

        $service = null;

        if(!$newInstance)
        {
            if(array_key_exists($serviceName, $this->staticServiceInstances))
            {
                $service = $this->staticServiceInstances[$serviceName];    
            }
            
            if(!$service)
            {
                $service = $this->getServiceInstance($serviceDefinition);

                $this->configureService($service, $serviceDefinition);

                $this->staticServiceInstances[$serviceName] = $service;
            }
        }
        else
        {
            $service = $this->getServiceInstance($serviceDefinition);

            $this->configureService($service, $serviceDefinition);
        }

        return $service;
    }

    protected function getServiceInstance(ServiceDefinition $serviceDefinition)
    {
        if(!$serviceDefinition)
        {
            throw new ServiceNotFoundException('Service with name: "' . $serviceName . '" is not registered.');
        }

        $className = $serviceDefinition->className;
        
        if($serviceDefinition->constructArguments)
        {
            if(is_array($serviceDefinition->constructArguments))
            {
                $reflection = new \ReflectionClass($className);

                $processedArguments = [];
                array_walk($serviceDefinition->constructArguments, $this->argumentsArrayWalkCallback, $processedArguments);

                return $reflection->newInstanceArgs($processedArguments);
            }
            else
            {
                return new $className($this->getRealArgumentValue($serviceDefinition->constructArguments));
            }
        }
        else
        {
            return new $className;
        }
    }

    protected function configureService($serviceInstance, ServiceDefinition $serviceDefinition)
    {
        if($serviceDefinition->methodArguments)
        {
            foreach($serviceDefinition->methodArguments as $key => $argumentValue)
            {   
                if(is_array($argumentValue))
                {
                    $processedArguments = [];
                    array_walk($argumentValue, $this->argumentsArrayWalkCallback, $processedArguments);

                    call_user_func_array(
                        array($serviceInstance, $key),
                        $processedArguments
                    );
                }
                else
                {
                    $serviceInstance->$key($this->getRealArgumentValue($argumentValue));
                }
            }
        }
    }

    protected function argumentsArrayWalkCallback($value, $key, $finalArgumentsList)
    {
        $finalArgumentsList[] = $this->getRealArgumentValue($value);   
    }

    protected function getRealArgumentValue($argumentValue)
    {
        if(strpos($argumentValue, '@') === false)
        {
            return $argumentValue;
        }
        else
        {
            return $this->getService(ltrim($argumentValue, '@'));
        }
    }
}
