<?php

namespace Jaggaer\Core\Factory;

use Jaggaer\Core\Factory\BaseCamelCaseFactory;
use Jaggaer\Core\DependencyInjection\IDependencyContainer;

class BaseTableGatewayFactory extends BaseCamelCaseFactory
{
    protected const DEFAULT_CONNECTION_NAME = 'default';

    private $databaseConfig;
    private $dependencyContainer;

    protected $databaseConnectionsList;
    
    public function __construct()
    {
        $this->databaseConnectionsList = [];
    }

    public function setDependencyContainer(IDependencyContainer $container)
    {
        $this->dependencyContainer = $container;
    }

    public function getDependencyContainer()
    {
        return $this->dependencyContainer;
    }

    protected function createDatabaseConnection($name = self::DEFAULT_CONNECTION_NAME)
    {
        if(array_key_exists($name, $this->databaseConnectionsList))
        {
            $connection = $this->databaseConnectionsList[$name];
        }
        else
        {
            $config = $this->getDependencyContainer()->getProperty('database_config')[$name];
            $connection = $this->getDependencyContainer()->getService('database_connection_factory')
                                                         ->createPDO($config);

            $this->databaseConnectionsList[$name] = $connection;
        }

        return $connection;
    }
}
