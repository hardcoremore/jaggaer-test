<?php

namespace Jaggaer\Core\Factory;

class BaseCamelCaseFactory
{
    protected $methodPrefix = 'create';
    protected $methodPostfix = '';

    public function get($name)
    {
        $methodName = $this->methodPrefix;
        $methodPieces = explode('_', $name);

        for($i = 0, $len = count($methodPieces); $i < $len; $i++)
        {
            $methodName .= ucfirst($methodPieces[$i]);
        }

        $methodName .= $this->methodPostfix;

        return $this->$methodName();
    }
}