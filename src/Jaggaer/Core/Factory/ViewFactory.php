<?php

namespace Jaggaer\Core\Factory;

use Jaggaer\Core\Factory\BaseCamelCaseFactory;

use Jaggaer\Core\View\JsonView;
use Jaggaer\Core\View\HtmlView;

class ViewFactory extends BaseCamelCaseFactory
{
    public function __construct()
    {
        $this->methodPostfix = 'View';
    }

    public function createJsonView()
    {
        return new JsonView();
    }

    public function createHtmlView()
    {
        return new HtmlView();
    }
}