<?php

namespace Jaggaer\Core\Factory;

use Jaggaer\Core\Factory\BaseCamelCaseFactory;

use Jaggaer\Core\Template\BaseHtmlTemplate;
use Jaggaer\Core\Template\FormTemplate;
use Jaggaer\Core\Template\TextInputFormItemTemplate;
use Jaggaer\Core\Template\TextAreaInputFormItemTemplate;

class BaseTemplateFactory extends BaseCamelCaseFactory
{
    public function __construct()
    {
        $this->methodPostfix = 'Template';
    }

    public function createHtmlTemplate()
    {
        return new BaseHtmlTemplate();
    }

    public function createFormTemplate()
    {
        return new FormTemplate($this);
    }

    public function createTextInputFormItemTemplate()
    {
        return new TextInputFormItemTemplate($this);
    }

    public function createTextAreaInputFormItemTemplate()
    {
        return new TextAreaInputFormItemTemplate($this);
    }
}