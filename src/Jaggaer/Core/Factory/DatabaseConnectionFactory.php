<?php

namespace Jaggaer\Core\Factory;

use Jaggaer\Core\Database\DatabaseConnectionException;

class DatabaseConnectionFactory
{
    public function createPDO(array $config)
    {
        try
        {
            $conn = new \PDO(
                'mysql:host=' . $config['host'] . ';dbname=' . $config['database'] . ';port=' . $config['port'],
                $config['username'],
                $config['password']
            );

            return $conn;
        }
        catch(\PDOException $exception)
        {
            throw new DatabaseConnectionException('Could not connect to database');
        }
    }
}