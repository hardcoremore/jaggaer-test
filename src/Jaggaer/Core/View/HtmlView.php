<?php

namespace Jaggaer\Core\View;

use Jaggaer\Core\Template\BaseHtmlTemplate;

class HtmlView extends BaseView
{
    protected $template;

    public function __construct()
    {
        parent::__construct();

        $this->mainHeaders[] = 'Content-type: text/html; charset=utf-8';
    }

    public function setTemplate(BaseHtmlTemplate $template)
    {
        $this->template = $template;

        return $this;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function getFormattedOutputData()
    {
        return $this->template->compile()->toString();
    }
}
