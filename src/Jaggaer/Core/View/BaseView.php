<?php

namespace Jaggaer\Core\View;

use Jaggaer\Core\View\IView;

abstract class BaseView implements IView
{
    protected $viewData;
    protected $statusCode;

    protected $mainHeaders;
    protected $userHeaders;

    public function __construct()
    {
        $this->mainHeaders = [];
        $this->statusCode = 200;
    }

    public function setViewData($data)
    {
        $this->viewData = $data;

        return $this;
    }

    public function getViewData()
    {
        return $this->viewData;
    }

    public function setStatusCode($code)
    {
        $this->statusCode = $code;

        return $this;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setUserHeaders(array $headers)
    {
        $this->userHeaders = $headers;

        return $this;
    }

    public function getUserHeaders()
    {
        return $this->userHeaders;
    }

    public function getAllHeaders()
    {
        if($this->userHeaders && count($this->userHeaders) > 0)
        {
            return array_merge($this->mainHeaders, $this->userHeaders);
        }
        else
        {
            return $this->mainHeaders;   
        }
    }

    public function render()
    {
        http_response_code($this->statusCode);
        $this->outputHeaders();
        echo $this->getFormattedOutputData();
    }

    public function outputHeaders()
    {
        $allHeaders = $this->getAllHeaders();
        $len = count($allHeaders);

        for($i = 0; $i < $len; $i++)
        {
            header($allHeaders[$i]);
        }
    }

    abstract public function getFormattedOutputData();
}