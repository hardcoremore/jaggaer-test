<?php

namespace Jaggaer\Core\View;

interface IView
{
    function setViewData($data);
    function getViewData();

    function setStatusCode($code);
    function getStatusCode();

    function setUserHeaders(array $headers);
    function getUserHeaders();

    function getAllHeaders();

    function getFormattedOutputData();
}