<?php

namespace Jaggaer\Core\View;

class JsonView extends BaseView
{
    public function __construct()
    {
        parent::__construct();

        $this->mainHeaders[] = 'Content-Type: application/json; charset=utf-8';
    }

    public function getFormattedOutputData()
    {
        return json_encode($this->getViewData());
    }
}