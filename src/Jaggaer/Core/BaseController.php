<?php

namespace Jaggaer\Core;

use Jaggaer\Core\DependencyInjection\IDependencyContainer;

class BaseController
{
    protected $dependencyContainer;

    public function setDependencyContainer(IDependencyContainer $container)
    {
        $this->dependencyContainer = $container;
    }

    public function getDependencyContainer()
    {
        return $this->dependencyContainer;
    }

    public function getModel($name, $namespace = null)
    {
        return $this->getDependencyContainer()->getService('model_factory')
                                              ->get($name);
    }

    public function getView($name, $namespace = null)
    {
        return $this->getDependencyContainer()->getService('view_factory')
                                              ->get($name);
    }

    public function getTemplate($name, $namespace = null)
    {
        return $this->getDependencyContainer()->getService('template_factory')
                                              ->get($name);
    }
}
