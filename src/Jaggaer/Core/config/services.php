<?php

use Jaggaer\Core\DependencyInjection\ServiceDefinition;

return [

    'database_connection_factory' => new ServiceDefinition('Jaggaer\Core\Factory\DatabaseConnectionFactory'),

    'template_factory' => new ServiceDefinition('Jaggaer\Core\Factory\BaseTemplateFactory'),
    'view_factory' => new ServiceDefinition('Jaggaer\Core\Factory\ViewFactory')
];
