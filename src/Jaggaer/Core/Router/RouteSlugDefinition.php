<?php

namespace Jaggaer\Core\Router;


class RouteSlugDefinition
{
    public $name;
    public $isStatic;
    
    public $isOptional;

    public $patternMatch;

    public function __construct($name, $isStatic = true, $isOptional = false, $patternMatch = '')
    {
        $this->name = $name;
        
        $this->isStatic = $isStatic;
        $this->isOptional = $isOptional;

        $this->patternMatch = $patternMatch;
    }
}
