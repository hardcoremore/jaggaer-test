<?php

namespace Jaggaer\Core\Router;

use Jaggaer\Core\Router\RouteSlugDefinition;
use Jaggaer\Core\Router\InvalidRouteSlugDefinitionException;

class RouteDefinition
{
    const DIGIT_ONLY = '/^[0-9]+$/';

    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';

    protected $type;

    protected $slugCount;
    protected $requiredSlugCount;

    protected $optionalSlugStartIndex;
    protected $slugList;

    protected $controllerClass;
    protected $controllerMethod;

    public function __construct($type = GET)
    {
        $this->type = $type;

        $this->slugCount = 0;
        $this->requiredSlugCount = 0;

        $this->optionalSlugStartIndex = 0;

        $this->slugListslugList = [];

        return $this;
    }

    public static function instance($type)
    {
        return new RouteDefinition($type);
    }

    public function getType()
    {
        return $this->type;   
    }

    public function getSlugCount()
    {
        return $this->slugCount;
    }

    public function getRequiredSlugCount()
    {
        return $this->requiredSlugCount;
    }

    public function getSlugList()
    {
        return $this->slugList;
    }

    public function getSlugAt($index)
    {
        if($index < $this->slugCount)
        {
            return $this->slugList[$index];
        }
        else
        {
            return null;
        }
    }

    public function addSlug($name, $isOptional = false, $patternMatch = '')
    {
        $this->optionalSlugValidationCheck($isOptional);

        $this->slugList[] = new RouteSlugDefinition($name, false, $isOptional, $patternMatch);
        $this->slugCount++;

        if(!$isOptional)
        {
            $this->requiredSlugCount++;
        }

        return $this;
    }

    protected function optionalSlugValidationCheck($isOptional)
    {
        if($isOptional)
        {
            if($this->slugCount > 0)
            {
                $this->optionalSlugStartIndex = 1;
            }
            else
            {
                throw new InvalidRouteSlugDefinitionException('First slug can not be optional');
            }
        }
        else if($this->optionalSlugStartIndex > 0)
        {
            throw new InvalidRouteSlugDefinitionException('You can not define required slug after optional slug is defined');
        }
    }

    public function setControllerClass($name)
    {
        $this->controllerClass = $name;

        return $this;
    }

    public function getControllerClass()
    {
        return $this->controllerClass;
    }

    public function setControllerMethod($name)
    {
        $this->controllerMethod = $name;

        return $this;
    }

    public function getControllerMethod()
    {
        return $this->controllerMethod;
    }
}
