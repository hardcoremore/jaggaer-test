<?php

namespace Jaggaer\JaggaerTree\Template;

use Jaggaer\Core\Template\BaseHtmlTemplate;

class AppMainTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Jaggaer Test</title>
</head>

<body class="test-123">
{%mainLayout%}
</body>

</html>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}