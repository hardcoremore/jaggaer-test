<?php

namespace Jaggaer\JaggaerTree\Template;

use Jaggaer\JaggaerTree\Template\BasePageTemplate;

class JaggaerTreePageTemplate extends BasePageTemplate
{
    private $template = <<<EOF
    <h1 style="color:red">This is Full Jaggaer Tree rendered.</h1>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}