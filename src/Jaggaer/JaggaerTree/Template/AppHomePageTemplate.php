<?php

namespace Jaggaer\JaggaerTree\Template;

use Jaggaer\JaggaerTree\Template\BasePageTemplate;

class AppHomePageTemplate extends BasePageTemplate
{
    private $template = <<<EOF
<h1 style="color:blue">Welcome to Jaggaer Tree</h1>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}
