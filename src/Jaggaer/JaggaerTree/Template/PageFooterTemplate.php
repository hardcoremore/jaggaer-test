<?php

namespace Jaggaer\JaggaerTree\Template;

use Jaggaer\Core\Template\BaseHtmlTemplate;

class PageFooterTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
    <h2>Jaggaer Tree Footer</h2>
    <a href="/aboutUs">About Us</a>
    <a href="/terms">Terms And Conditions</a>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}

