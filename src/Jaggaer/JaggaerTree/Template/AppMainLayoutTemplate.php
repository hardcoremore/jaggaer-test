<?php

namespace Jaggaer\JaggaerTree\Template;

use Jaggaer\Core\Template\BaseHtmlTemplate;

class AppMainLayoutTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF

    {%header%}

    <div class="main-container">
        {%pageContent%}
    </div>

    {%footer%}

EOF;

    public function getTemplate()
    {
        return $this->template;
    }

    public function compile()
    {
        $this->addChildTemplate('header', $this->templateFactory->get('page_header'));
        $this->addChildTemplate('footer', $this->templateFactory->get('page_footer'));

        $mainTemplate = $this->templateFactory->get('app_main');
        $mainTemplate->addChildTemplate('mainLayout', $this);
        $mainTemplate->compile();

        return $mainTemplate;
    }
}
