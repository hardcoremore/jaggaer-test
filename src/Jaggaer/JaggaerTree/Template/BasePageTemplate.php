<?php

namespace Jaggaer\JaggaerTree\Template;

use Jaggaer\Core\Template\BaseHtmlTemplate;

class BasePageTemplate extends BaseHtmlTemplate
{
    protected $headerData;
    protected $pageContentData;

    public function setHeaderData(array $data)
    {
        $this->headerData = $data;
    }

    public function getHeaderData()
    {
        return $this->headerData;
    }

    public function compile()
    {
        $main = $this->templateFactory->get('app_main_layout');

        $mainLayout = $main->compile()->getChildTemplate('mainLayout');

        $mainLayout->getChildTemplate('header')->setProperties($this->headerData);

        $mainLayout->addChildTemplate('pageContent', $this);

        return $main;
    }
}