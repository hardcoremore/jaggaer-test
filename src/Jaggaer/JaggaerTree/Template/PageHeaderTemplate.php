<?php

namespace Jaggaer\JaggaerTree\Template;

use Jaggaer\Core\Template\BaseHtmlTemplate;

class PageHeaderTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
    <h2>Welcome to Jaggaer Tree, <span style="color:purple">{{firstName}} {{lastName}}<span></h2>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}

