<?php

use Jaggaer\Core\DependencyInjection\ServiceDefinition;

return [

    'table_gateway_factory' => new ServiceDefinition(
        'Jaggaer\JaggaerTree\Factory\JaggaerTableGatewayFactory',
        null,
        [
            'setDependencyContainer' => '@dependency_container'
        ]
    ),

    'model_factory' => new ServiceDefinition(
        'Jaggaer\JaggaerTree\Factory\ModelFactory',
        null,
        [
            'setTableGatewayFactory' => '@table_gateway_factory'
        ]
    ),

    'template_factory' => new ServiceDefinition('Jaggaer\JaggaerTree\Factory\AppTemplateFactory')
];
