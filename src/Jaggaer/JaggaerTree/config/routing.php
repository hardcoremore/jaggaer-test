<?php

use Jaggaer\Core\Router\RouteDefinition;

CONST JAGGAER_TREE_ROUTE = 'jaggaertree';
CONST JAGGAER_TREE_CONTROLLER = 'Jaggaer\JaggaerTree\Controller\MyTreeViewController';

return [

    RouteDefinition::instance(RouteDefinition::GET)->addSlug(JAGGAER_TREE_ROUTE)
                                                   ->addSlug('full')
                                                   ->addSlug('expandNodeId', true, RouteDefinition::DIGIT_ONLY)
                                                   ->setControllerClass(JAGGAER_TREE_CONTROLLER)
                                                   ->setControllerMethod('showCompleteTree'),

    RouteDefinition::instance(RouteDefinition::GET)->addSlug(JAGGAER_TREE_ROUTE)
                                                   ->addSlug('lazy')
                                                   ->setControllerClass(JAGGAER_TREE_CONTROLLER)
                                                   ->setControllerMethod('showAjaxTree'),

    RouteDefinition::instance(RouteDefinition::GET)->addSlug(JAGGAER_TREE_ROUTE)
                                                   ->addSlug('node')
                                                   ->addSlug('nodeId', false, RouteDefinition::DIGIT_ONLY)
                                                   ->setControllerClass(JAGGAER_TREE_CONTROLLER)
                                                   ->setControllerMethod('fetchAjaxTreeNode')
];
