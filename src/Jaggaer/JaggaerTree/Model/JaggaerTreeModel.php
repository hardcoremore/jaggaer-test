<?php

namespace Jaggaer\JaggaerTree\Model;

use Jaggaer\Core\BaseModel;

class JaggaerTreeModel extends BaseModel
{
    public function getNodeChildren($nodeId)
    {
        $node = $this->table->getById($nodeId);

        if($node && $node['parent_entry_id'] > 0)
        {
            return $this->table->search(['parent_entry_id' => $node['parent_entry_id']]);
        }
        else
        {
            return [];
        }
    }
}