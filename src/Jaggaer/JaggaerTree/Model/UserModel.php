<?php

namespace Jaggaer\JaggaerTree\Model;

use Jaggaer\Core\BaseModel;

class UserModel extends BaseModel
{
    public function getUserData()
    {
        return [
            'id' => 87,
            'firstName' => 'Časlav',
            'lastName' => 'Šabani',
            'username' => 'Chaky',
            'email' => 'caslav.sabani@gmail.com'
        ];
    }
}