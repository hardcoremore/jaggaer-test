<?php

namespace Jaggaer\JaggaerTree\Factory;

use Jaggaer\Core\Factory\BaseTableGatewayFactory;

use Jaggaer\JaggaerTree\Database\JaggaerTreeTable;

class JaggaerTableGatewayFactory extends BaseTableGatewayFactory
{
    public function createJaggaerTreeTable()
    {
        $connection = $this->createDatabaseConnection();

        $table = new JaggaerTreeTable();
        $table->setTableName('tree_entry');
        $table->setIdColumnName('entry_id');
        $table->setConnection($connection);

        return $table;
    }
}
