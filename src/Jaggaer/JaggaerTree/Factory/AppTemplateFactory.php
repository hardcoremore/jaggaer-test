<?php

namespace Jaggaer\JaggaerTree\Factory;

use Jaggaer\Core\Factory\BaseTemplateFactory;

use Jaggaer\JaggaerTree\Template\AppMainTemplate;

use Jaggaer\JaggaerTree\Template\PageHeaderTemplate;
use Jaggaer\JaggaerTree\Template\PageFooterTemplate;
use Jaggaer\JaggaerTree\Template\AppMainLayoutTemplate;

use Jaggaer\JaggaerTree\Template\AppHomePageTemplate;
use Jaggaer\JaggaerTree\Template\JaggaerTreePageTemplate;

class AppTemplateFactory extends BaseTemplateFactory
{
    public function createAppMainTemplate()
    {
        return new AppMainTemplate($this);
    }

    public function createPageHeaderTemplate()
    {
        return new PageHeaderTemplate($this);
    }

    public function createPageFooterTemplate()
    {
        return new PageFooterTemplate($this);
    }

    public function createAppMainLayoutTemplate()
    {
        return new AppMainLayoutTemplate($this);
    }


    public function createAppHomePageTemplate()
    {
        return new AppHomePageTemplate($this);
    }

    

    public function createJaggaerTreePageTemplate()
    {
        return new JaggaerTreePageTemplate($this);   
    }
}