<?php

namespace Jaggaer\JaggaerTree\Factory;

use Jaggaer\Core\Factory\BaseCamelCaseFactory;

use Jaggaer\JaggaerTree\Factory\JaggaerTableGatewayFactory;

use Jaggaer\JaggaerTree\Model\UserModel;
use Jaggaer\JaggaerTree\Model\JaggaerTreeModel;

class ModelFactory extends BaseCamelCaseFactory
{
    private $tableGatewayFactory;

    public function __construct()
    {
        $this->methodPostfix = 'Model';
    }

    public function setTableGatewayFactory(JaggaerTableGatewayFactory $factory)
    {
        $this->tableGatewayFactory = $factory;
    }

    public function getTableGatewayFactory()
    {
        return $this->tableGatewayFactory;
    }

    public function createUserModel()
    {
        return new UserModel();
    }

    public function createJaggaerTreeModel()
    {
        $table = $this->tableGatewayFactory->createJaggaerTreeTable();

        $model = new JaggaerTreeModel();
        $model->setTable($table);

        return $model;
    }
}