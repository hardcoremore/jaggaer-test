<?php

namespace Jaggaer\JaggaerTree\Controller;

use Jaggaer\JaggaerTree\Controller\AbstractTreeViewController;

class MyTreeViewController extends AbstractTreeViewController
{
    public function homePage()
    {
        $userData = $this->getModel('user')->getUserData();

        $template = $this->getTemplate('app_home_page');
        $template->setHeaderData($userData);

        return $this->getView('html')->setTemplate($template);
    }

    public function showCompleteTree($expandNodeId = null)
    {
        $userData = $this->getModel('user')->getUserData();
        
        $template = $this->getTemplate('jaggaer_tree_page');
        $template->setHeaderData($userData);

        $treeData = $this->getModel('jaggaer_tree')->getById(10);

        plog('DATA:');
        plog($treeData);

        return $this->getView('html')->setTemplate($template);
	}
	
	public function showAjaxTree()
    {
	}
	
	public function fetchAjaxTreeNode($entryId)
    {
        return $this->getView('json')->setViewData(
            $this->getModel('jaggaer_tree')->getNodeChildren($entryId)
        );
	}
}
