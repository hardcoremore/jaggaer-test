<?php

namespace Jaggaer\JaggaerTree\Controller;

use Jaggaer\Core\BaseController;

abstract class AbstractTreeViewController extends BaseController
{
    abstract public function showCompleteTree($expandNodeId);	 
	abstract public function showAjaxTree();
	abstract public function fetchAjaxTreeNode($entry_id);
}