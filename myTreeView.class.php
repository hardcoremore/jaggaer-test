<?php

/**
 * Implement your code here
 * Feel free to remove the echos :)
 */

class myTreeView extends abstractTreeView {
	
        public function showCompleteTree() {
			echo 'Show Complete Tree<br>';
		}
		
		public function showAjaxTree() {
			echo 'Show Ajax Tree<br>';
		}
		
		public function fetchAjaxTreeNode($entry_id) {
			echo 'fetchAjaxTreeNode for entry_id ('.$entry_id.')<br>';
		}
}