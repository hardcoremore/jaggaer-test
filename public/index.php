<?php

error_reporting(E_ALL);
ini_set('display_startup_errors', '1');
ini_set('display_errors', '1');

require_once '../autoload.php';

define('APP_NAMESPACE', 'Jaggaer/JaggaerTree');

function plog($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function globalExceptionHandler($exception)
{
    echo '<h2>Exception</h2>';
    plog(explode(',',$exception->getMessage()));
    plog($exception->getTraceAsString());
}

set_exception_handler('globalExceptionHandler');

use Jaggaer\Core\Router\SimpleRouter;
use Jaggaer\Core\DependencyInjection\DependencyContainer;
use Jaggaer\Core\View\InvalidViewException;
use Jaggaer\Core\Router\RouteDefinition;

$routes = require_once '../' . BASE_APP_DIR . '/'. APP_NAMESPACE . '/config/routing.php';

$homeRoute = RouteDefinition::instance(RouteDefinition::GET)->setControllerClass(JAGGAER_TREE_CONTROLLER)
                                                            ->setControllerMethod('homePage');

$router = new SimpleRouter();
$router->setRoutes($routes);
$router->setHomeRoute($homeRoute);


if(strpos(php_sapi_name(), 'cli') === false)
{
    $router->processHTTP($_SERVER, $_GET, $_POST);
}
else
{
    $router->processCLI($argv);
}

$matchedRoute = $router->getMatchedRoute();

$controllerClass = $matchedRoute->getControllerClass();
$controllerMethod = $matchedRoute->getControllerMethod();

$controller = new $controllerClass();

$baseServicesConfig = require_once '../' . BASE_APP_DIR . 'Jaggaer/Core/config/services.php';
$appServicesConfig = require_once '../' . BASE_APP_DIR . '/'. APP_NAMESPACE . '/config/services.php';
$databaseConfig = require_once '../' . BASE_APP_DIR . '/'. APP_NAMESPACE . '/config/database.php';

$container = new DependencyContainer();

$allServices = array_merge($baseServicesConfig, $appServicesConfig);

foreach($allServices as $key => $value)
{
    if(strpos($key, '@') === false)
    {
        $container->registerService($key, $value);
    }
    else
    {
        $container->setProperty(ltrim($key, '@'), $value);
    }
}

$container->setService('dependency_container', $container);
$container->setProperty('database_config', $databaseConfig);

$controller->setDependencyContainer($container);

if(count($router->getVariableSlugs()) > 0)
{
    $view = call_user_func_array(array($controller, $controllerMethod), $router->getVariableSlugs());
}
else
{
    $view = $controller->$controllerMethod();
}

if($view)
{
    $view->render();
}
else
{
    throw new InvalidViewException('Controller returned invalid view.');
}

        